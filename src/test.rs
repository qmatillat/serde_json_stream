use super::*;

#[test]
fn empty() {
    let array = r#"[] {}"#;
    let mut src = serde_json::de::StrRead::new(&array);

    let mut root = StreamDeserializer::new(&mut src);
    let mut array = root.enter_array().unwrap().1;
    assert_eq!(array.hint_next_value().unwrap(), NextHint::End);
    let mut it = array.iter::<()>();
    assert!(it.next().is_none());

    array.end().unwrap();

    let mut map = root.enter_map().unwrap().1;
    let mut it = map.iter::<()>();
    assert!(it.next().is_none());

    map.end().unwrap();

    root.end().unwrap();
}

#[test]
fn simple_array() {
    let array = r#" [ 1, 2, 7, "toto", 8 ] "#;
    let mut src = serde_json::de::StrRead::new(&array);

    let mut root = StreamDeserializer::new(&mut src);

    let ((), mut array) = root.enter_array().unwrap();

    assert_eq!(array.next_value().unwrap(), (0, 1));
    assert_eq!(array.next_value().unwrap(), (1, 2));
    assert_eq!(array.next_value().unwrap(), (2, 7));
    assert_eq!(array.next_value().unwrap(), (3, "toto"));
    assert_eq!(array.next_value().unwrap(), (4, 8));

    array.end().unwrap();
    root.end().unwrap();
}

#[test]
fn nested() {
    let array = r#" [ 1, 2, { "a" : 7, "b": 8 }, 4 ] 5 [ 1, 2 ] "#;
    let mut src = serde_json::de::StrRead::new(&array);

    let mut root = StreamDeserializer::new(&mut src);

    let ((), mut array) = root.enter_array().unwrap();

    assert_eq!(array.next_value().unwrap(), (0, 1));
    assert_eq!(array.next_value().unwrap(), (1, 2));

    {
        let (id, mut map) = array.enter_map().unwrap();
        assert_eq!(id, 2);

        assert_eq!(map.next_value().unwrap(), ("a".to_string(), 7));
        assert_eq!(map.next_value().unwrap(), ("b".to_string(), 8));
        map.end().unwrap();
    }

    assert_eq!(array.next_value().unwrap(), (3, 4));

    array.end().unwrap();

    assert_eq!(root.next_value().unwrap(), ((), 5));

    let ((), mut array) = root.enter_array().unwrap();
    assert_eq!(array.next_value().unwrap(), (0, 1));
    assert_eq!(array.next_value().unwrap(), (1, 2));
    array.end().unwrap();

    root.end().unwrap();
}

#[test]
fn iter() {
    let array = r#" [ 1, 2, 3, 4, 5 ] 9 "#;
    let mut src = serde_json::de::StrRead::new(&array);

    let mut root = StreamDeserializer::new(&mut src);

    let ((), mut array) = root.enter_array().unwrap();
    let mut it = array.iter();

    assert_eq!(it.next().unwrap().unwrap(), (0, 1));
    assert_eq!(it.next().unwrap().unwrap(), (1, 2));
    assert_eq!(it.next().unwrap().unwrap(), (2, 3));
    assert_eq!(it.next().unwrap().unwrap(), (3, 4));
    assert_eq!(it.next().unwrap().unwrap(), (4, 5));
    assert!(it.next().is_none());

    array.end().unwrap();

    assert_eq!(root.next_value().unwrap(), ((), 9));

    root.end().unwrap();
}

#[test]
fn iter_root() {
    let array = r#" 1 2 3 4 5 "#;
    let mut src = serde_json::de::StrRead::new(&array);

    let mut root = StreamDeserializer::new(&mut src);

    let mut it = root.iter();

    assert_eq!(it.next().unwrap().unwrap(), ((), 1));
    assert_eq!(it.next().unwrap().unwrap(), ((), 2));
    assert_eq!(it.next().unwrap().unwrap(), ((), 3));
    assert_eq!(it.next().unwrap().unwrap(), ((), 4));
    assert_eq!(it.next().unwrap().unwrap(), ((), 5));
    assert!(it.next().is_none());

    root.end().unwrap();
}

#[test]
fn iter_map() {
    let array = r#" { "a": 1, "b": 2  } 9 "#;
    let mut src = serde_json::de::StrRead::new(&array);

    let mut root = StreamDeserializer::new(&mut src);

    let ((), mut map) = root.enter_map().unwrap();
    let mut it = map.iter();

    assert_eq!(it.next().unwrap().unwrap(), ("a".to_string(), 1));
    assert_eq!(it.next().unwrap().unwrap(), ("b".to_string(), 2));
    assert!(it.next().is_none());

    map.end().unwrap();

    assert_eq!(root.next_value().unwrap(), ((), 9));

    root.end().unwrap();
}
