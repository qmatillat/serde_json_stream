use crate::{Expected, Result};
use serde_json::de::Read;

/// Root parser (space separated)
mod root;
pub use root::Root;

/// Object parser
mod object;
pub use object::Object;

/// Array parser
mod array;
pub use array::Array;

/// Type of the next element
#[derive(Debug, Eq, PartialEq)]
pub enum NextHint {
    /// The next element is a value (number, strings, ...).
    /// This element can't be streamed.
    Value,

    /// This is the end of the current container (array / object)
    End,

    /// Unknown next element.
    /// No matter what method you'll call next, it's very likely that it will throw an error
    Unknown,
}

/// Type of object being deserialized (Root, Array, Object)
pub trait Type: Default {
    /// Type of the index
    type Index;

    /// Try to see what is the type of the next element
    /// 
    /// # Errors
    /// Returns underlmying error from `Reader`
    fn whats_next<'de, R: Read<'de>>(&mut self, r: &mut R) -> Result<NextHint>;

    /// Parse the begining of the structure ('[' for array and '{' for object)
    /// 
    /// # Errors
    /// Returns an error if this is not the expected start.
    /// Must leave the parser in the previous state.
    fn parse_opening<'de, R: Read<'de>>(r: &mut R) -> Result<()>;

    /// Parse the end of the structure (']' for array and '}' for object)
    ///
    /// # Errors
    /// Returns an error if this is not the expected end.
    /// Must leave the parser in the previous state.
    fn parse_end<'de, R: Read<'de>>(&mut self, r: &mut R) -> Result<()>;

    /// Parse the key
    ///
    /// # Errors
    /// When an error is returned, the parser might be in an inconsistent state,
    /// further parsing might fail
    fn parse_key<'de, R: Read<'de>>(&mut self, r: &mut R) -> Result<Self::Index>;
}

/// Skip all whitespace, and return the next char
/// Return `None` if EOF
fn parse_whitespace<'de, R: Read<'de>>(read: &mut R) -> Result<Option<u8>> {
    loop {
        match read.peek()? {
            Some(b' ') | Some(b'\n') | Some(b'\t') | Some(b'\r') => read.discard(),
            Some(c) => return Ok(Some(c)),
            None => return Ok(None),
        }
    }
}

/// Expect the next char to be a specific value
///
/// # Errors
/// If the next char (excluding whitespace) does not match, return `StreamingError::Expected`
fn expect_char<'de, R: Read<'de>>(r: &mut R, expected: Expected) -> Result<()> {
    let expected_char = match expected {
        Expected::BeginArray => b'[',
        Expected::BeginObject => b'{',
        Expected::EndArray => b']',
        Expected::EndObject => b'}',
        Expected::NameSeparator => b':',
        Expected::ValueSeparator => b',',

        Expected::Value => 0,
    };

    match parse_whitespace(r)? {
        Some(c) if c == expected_char => {
            r.discard();
            Ok(())
        }
        Some(found) => Err(expected.into_error(r, found)),
        None => Err(expected.into_error_eof()),
    }
}
