use crate::{Expected, Result};
use serde_json::de::Read;

use super::{expect_char, parse_whitespace, Type, NextHint};

/// Array parser
#[derive(Default)]
pub struct Array {
    /// Number of element already parsed (used for index)
    pos: usize,
}

impl Array {
    /// Is it the first element.
    /// Used to know if we expect a comma
    const fn first(&self) -> bool {
        self.pos == 0
    }
}

impl Type for Array {
    type Index = usize;

    #[inline]
    fn whats_next<'de, R: Read<'de>>(&mut self, r: &mut R) -> Result<NextHint> {
        Ok(match parse_whitespace(r)? {
            Some(b']') => NextHint::End,
            Some(b',') => if self.first() { NextHint::Unknown } else {NextHint::Value },
            Some(_) if self.first() => NextHint::Value,
            Some(_) | None => NextHint::Unknown,
        })
    }

    #[inline]
    fn parse_opening<'de, R: Read<'de>>(r: &mut R) -> Result<()> {
        expect_char(r, Expected::BeginArray)
    }

    #[inline]
    fn parse_end<'de, R: Read<'de>>(&mut self, r: &mut R) -> Result<()> {
        expect_char(r, Expected::EndArray)
    }

    #[inline]
    fn parse_key<'de, R: Read<'de>>(&mut self, r: &mut R) -> Result<Self::Index> {
        let key = self.pos;
        if !self.first() {
            expect_char(r, Expected::ValueSeparator)?;
        }
        
        self.pos = self.pos.wrapping_add(1);

        Ok(key)
    }
}
