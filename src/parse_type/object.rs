use crate::{Expected, Result};
use serde::Deserialize;
use serde_json::de::Read;
use serde_json::{Deserializer};

use super::{expect_char, parse_whitespace, Type, NextHint};

/// Object parser
#[derive(Default)]
pub struct Object {
    /// Has the first element been parsed (used to know if we need a coma)
    first_done: bool,
}

impl Type for Object {
    type Index = String;

    #[inline]
    fn whats_next<'de, R: Read<'de>>(&mut self, r: &mut R) -> Result<NextHint> {
        Ok(match parse_whitespace(r)? {
            Some(b'}') => NextHint::End,
            Some(b',') => if !self.first_done {NextHint::Unknown } else { NextHint::Value },
            Some(_) if !self.first_done => NextHint::Value,
            Some(_) | None => NextHint::Unknown,
        })
    }

    #[inline]
    fn parse_opening<'de, R: Read<'de>>(r: &mut R) -> Result<()> {
        expect_char(r, Expected::BeginObject)
    }

    #[inline]
    fn parse_end<'de, R: Read<'de>>(&mut self, r: &mut R) -> Result<()> {
        expect_char(r, Expected::EndObject)
    }

    #[inline]
    fn parse_key<'de, R: Read<'de>>(&mut self, mut r: &mut R) -> Result<Self::Index> {
        // Parse separator if not on first value
        if self.first_done  {
            expect_char(r, Expected::ValueSeparator)?;
        }

        self.first_done = true;

        // Parse key
        let key = {
            let mut d = Deserializer::new(&mut r);
            String::deserialize(&mut d)?
        };

        expect_char(r, Expected::NameSeparator)?;

        Ok(key)
    }
}