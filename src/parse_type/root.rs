use crate::Result;
use serde_json::de::Read;

use super::{parse_whitespace, Type, NextHint};

/// Root parser
#[derive(Default)]
pub struct Root;

impl Type for Root {
    type Index = ();

    #[inline]
    fn whats_next<'de, R: Read<'de>>(&mut self, r: &mut R) -> Result<NextHint> {
        Ok(match parse_whitespace(r)? {
            Some(_) => NextHint::Value,
            None => NextHint::End,
        })
    }

    #[inline]
    fn parse_opening<'de, R: Read<'de>>(_: &mut R) -> Result<()> {
        Ok(())
    }

    #[inline]
    fn parse_end<'de, R: Read<'de>>(&mut self, _: &mut R) -> Result<()> {
        Ok(())
    }

    #[inline]
    fn parse_key<'de, R: Read<'de>>(&mut self, _: &mut R) -> Result<Self::Index> {
        Ok(())
    }
}
