use thiserror::Error;
use serde_json::de::Read;

/// Position where an error happened
#[derive(Debug)]
pub struct Position {
    /// Line of the error
    pub line: usize,

    /// Column of the error
    pub column: usize,
}

/// Expected value
#[derive(Debug)]
pub enum Expected {
    /// Begin of an array ('[')
    BeginArray,

    /// Begin of an object ('{')
    BeginObject,

    /// End of an array (']')
    EndArray,

    /// End of an object ('}')
    EndObject,

    /// Separator between name and value in object (':')
    NameSeparator,

    /// Separator between value (',')
    ValueSeparator,

    /// A value
    Value,
}

impl Expected {
    /// Convert an Expectation into an error
    pub(crate) fn into_error<'de, R: Read<'de>>(self, r: &R, found: impl Into<char>) -> StreamError {
        StreamError::Expected {
            expected: self,
            found: found.into(),
            position: {
                let pos = r.peek_position();
                Position {
                    line: pos.line,
                    column: pos.column,
                }
            },
        }
    }

    /// Convert an Expection into an `UnexpectedEof` error
    pub(crate) const fn into_error_eof(self) -> StreamError {
        StreamError::UnexpectedEof {
            expected: self,
        }
    }
}

/// Custom error type
#[allow(clippy::module_name_repetitions)]
#[derive(Error, Debug)]
pub enum StreamError {
    /// Underlying error from `Read`
    #[error("Inner error : {source}")]
    Internal {
        /// Inner error
        #[from]
        source: serde_json::Error,
    },

    /// Unexpected char reached
    #[error("Expected {expected:?} instead of {found:?} at {position:?}")]
    Expected {
        /// Expected value
        expected: Expected,

        /// Value found
        found: char,

        /// Position of the error
        position: Position,
    },

    /// EoF reached, instead of `expected`
    #[error("Expected {expected:?}, but reach end of file")]
    UnexpectedEof {
        /// Expected value
        expected: Expected,
    }
}

/// Default error type, used by most function
pub type Result<T> = std::result::Result<T, StreamError>;
