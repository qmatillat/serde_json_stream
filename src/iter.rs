use serde::Deserialize;
use serde_json::de::Read;

use crate::{ParseType, Result, StreamDeserializer, NextHint};

/// Iterator
pub struct Iter<'a, 'b, 'de, R, PT, D>
where
    R: Read<'de>,
    PT: ParseType,
    D: Deserialize<'de>,
{
    /// Deserializer
    pub(crate) deserializer: &'b mut StreamDeserializer<'a, 'de, R, PT>,

    /// Hold type to deserialize to
    pub(crate) item_type: std::marker::PhantomData<D>,
}

impl<'a, 'b, 'de, R, PT, D> Iterator for Iter<'a, 'b, 'de, R, PT, D>
where
    R: Read<'de>,
    PT: ParseType,
    D: Deserialize<'de>,
{
    type Item = Result<(PT::Index, D)>;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        match self.deserializer.hint_next_value() {
            Ok(NextHint::End) => None,
            Ok(_) => Some(self.deserializer.next_value()),
            Err(e) => Some(Err(e)),
        }
    }
}
