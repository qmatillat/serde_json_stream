//! Straming JSON

#![warn(
    clippy::all,
    clippy::restriction,
    clippy::pedantic,
    clippy::nursery,
    clippy::cargo
)]
#![allow(clippy::implicit_return)]

use serde::Deserialize;
use serde_json::de::Read;

/// Errors structure
mod error;
pub use error::{Expected, Result, StreamError};

/// Iterator related structure
pub mod iter;

/// Parser types (Root, Array and Object)
pub mod parse_type;
use parse_type::{Array, Object, Root, Type as ParseType};
pub use parse_type::NextHint;

/// Streaming Deserializer that expect value separated 
pub type RootStreamDeserializer<'a, 'de, R> = StreamDeserializer<'a, 'de, R, Root>;

/// Streaming Deserializer in an array
pub type ArrayStreamDeserializer<'a, 'de, R> = StreamDeserializer<'a, 'de, R, Array>;

/// Streaming Deserializer in an object 
pub type ObjectStreamDeserializer<'a, 'de, R> = StreamDeserializer<'a, 'de, R, Object>;

#[cfg(test)]
mod test;

/// Structure that allow streaming deserialization
pub struct StreamDeserializer<'a, 'de, R, T>
where
    R: Read<'de>,
    T: ParseType,
{
    /// Reader used for streaming deserialization
    src: &'a mut R,
    /// Type being decoded
    type_decoder: T,
    /// Lifetime marker
    lifetime: std::marker::PhantomData<&'de ()>,
}

impl<'a, 'de, R: Read<'de>> StreamDeserializer<'a, 'de, R, Root> {
    /// Create a new deserializer
    #[inline]
    pub fn new(src: &'a mut R) -> Self {

        #[allow(clippy::expect_used)]
        Root::parse_opening(src).expect("Root should not be able to fail");

        Self {
            src,
            type_decoder: Root::default(),
            lifetime: std::marker::PhantomData::default(),
        }
    }
}

impl<'a, 'de, R: Read<'de>, T: ParseType> StreamDeserializer<'a, 'de, R, T> {
    #[doc(hidden)]
    #[inline]
    fn inner_new<'b, TT: ParseType>(
        &'b mut self,
    ) -> Result<(T::Index, StreamDeserializer<'b, 'de, R, TT>)> {
        let idx = self.type_decoder.parse_key(self.src)?;

        TT::parse_opening(self.src)?;

        Ok((
            idx,
            StreamDeserializer {
                src: self.src,
                type_decoder: TT::default(),
                lifetime: std::marker::PhantomData::default(),
            },
        ))
    }

    /// Enter a sub-array
    ///
    /// # Errors
    /// Returns an error if this is not the start of an array.
    /// The parser will still be in an consistent state.
    #[inline]
    pub fn enter_array<'b>(
        &'b mut self,
    ) -> Result<(T::Index, StreamDeserializer<'b, 'de, R, Array>)> {
        self.inner_new()
    }

    /// Enter a sub-map
    ///
    /// # Errors
    /// Returns an error if this is not the start of an object/map.
    /// The pârser will still be in an consistent state.
    #[inline]
    pub fn enter_map<'b>(
        &'b mut self,
    ) -> Result<(T::Index, StreamDeserializer<'b, 'de, R, Object>)> {
        self.inner_new()
    }

    /// Parse the next value (without streaming capabilities)
    ///
    /// # Errors
    /// When an error is returned, the parser might be in an inconsistent state,
    /// further parsing might fail
    #[inline]
    pub fn next_value<D: Deserialize<'de>>(&mut self) -> Result<(T::Index, D)> {
        // self.type_decoder.parse_separator(self.src)?;
        let idx = self.type_decoder.parse_key(self.src)?;
        let mut de = serde_json::Deserializer::new(&mut self.src);
        let val = D::deserialize(&mut de)?;

        Ok((idx, val))
    }

    /// End the parsing of the current element
    ///
    /// # Errors
    /// Will return an error if couldn't parse the end of the element.
    /// See `can_end`
    #[inline]
    pub fn end(mut self) -> Result<()> {
        self.type_decoder.parse_end(self.src)?;

        Ok(())
    }

    /// Try to see what is the type of the next element
    /// 
    /// # Errors
    /// Returns underlmying error from `Reader`
    #[inline]
    pub fn hint_next_value(&mut self) -> Result<NextHint> {
        self.type_decoder.whats_next(self.src)
    }

    /// Turn a JSON deserializer into an iterator over values of type T.
    /// You still need to call `end`.
    #[inline]
    pub fn iter<'b, K: Deserialize<'de>>(&'b mut self) -> iter::Iter<'a, 'b, 'de, R, T, K> {
        iter::Iter {
            deserializer: self,
            item_type: std::marker::PhantomData::default(),
        }
    }
}
